#!/bin/env bash
# set -x

# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -euo pipefail

for subs in $(az account list -o tsv --query "[].id")
do
    az account set --subscription ${subs} 
    subName=$(az account show -o tsv --query "name")
    az group list -o tsv --query "sort_by([? !(contains(name,'DefaultResourceGroup')) && !(contains(name,'NetworkWatcherRG')) && !(contains(name,'AzureBackupRG'))].{Subs:'${subName}',Name:name,Location:location,Project:tags.project}, &Name)" | sed 's/\t/;/g'
done
