<sup>If you just came here looking for the solution, [here you have it](./listRG.sh)</sup>

# Advanced [^1] Queries on `az cli`

Every now and then, mostly due to the ISO27K, I have to create some reports about the Azure Resource Groups (RG) we are using internally but, including the cost center (the Azure subscription) related to each Resource Group (RG).

Every now and then I have to struggle with the `az cli` to create such kind of reports, so I've decided to create this "article", next time I won't need to struggle again and, additionally, may be some one else could take advantage of the time I expended fighting the Azure CLI.

## Basic Approach

The simplest approach to get the list of RGs on your Azure Subscription would be something like this:

```bash
az group list -o table
```

Plain and simple, the output of the previous command is as follows:

```
Name                        Location          Status
--------------------------  ----------------  ---------
flowable-Ahsh8At5           westeurope        Succeeded
DefaultResourceGroup-WEU    westeurope        Succeeded
flowable-deej1Ood           westeurope        Succeeded
flowable-Ahs6ja8u           westeurope        Succeeded
AzureBackupRG_westeurope_1  westeurope        Succeeded
flowable-AHohk4pu           westeurope        Succeeded
flowable-on8ooJai           westeurope        Succeeded
flowable-aiKai6Ku           switzerlandnorth  Succeeded
NetworkWatcherRG            brazilsouth       Succeeded
```
<sup>The name of the RGs has been changed to protect the innocents.</sup>

Good enough, but, wouldn't be nice to have the subscription in the report too?


## Get the subscription.

In order to take the subscription, we need to iterate over all our subscriptions, so, we need to do something slightly more complex. Let's add a loop to go through all those subscriptions.   
We can get the list of subscriptions using a very simple `az cli` command:

```bash
az account list
```

but we need just the `id` and we need to iterate over all of them, so

```bash
for subs in $(az account list -o tsv --query "[].id")
do
    # Switch to the next subscription on the loop
    az account set --subscription ${subs}
    az group list -o table
done
```

Ok, but this doesn't show the subscription on the list... how to do that.  
Well, we must modify the query to include the fields we want.  
Here starts the magic.

```bash
for subs in $(az account list -o tsv --query "[].id")
do
    # Switch to the next subscription on the loop
    az account set --subscription ${subs}
    az group list -o table --query "[].['${subs}',name,location]"
done
```
<sup>Mind the double quotes and single quotes if you don't want go crazy</sup>

Nice, now we have the basic information we want, including the subscription but, on the other hand the report looks weird since it mentions "Column1, Column2, Column3" instead of what we are looking for, E.G:

```
Column1                               Column2                     Column3
------------------------------------  --------------------------  ----------------
dxxxxxxe-1234-5678-9012-345678901234  flowable-Ahsh8At5           westeurope
dxxxxxxe-1234-5678-9012-345678901234  DefaultResourceGroup-WEU    westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-deej1Ood           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-Ahs6ja8u           westeurope
dxxxxxxe-1234-5678-9012-345678901234  AzureBackupRG_westeurope_1  westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-AHohk4pu           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-on8ooJai           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-aiKai6Ku           switzerlandnorth
dxxxxxxe-1234-5678-9012-345678901234  NetworkWatcherRG            brazilsouth
```
<sup>Subscription ID has been obfuscated to protect the innocents.</sup>

### Specify the column name

To rename the columns (AKA to get a dictionary instead of an array) when querying multiple values, we must use curly brackets `{}`.


```bash
for subs in $(az account list -o tsv --query "[].id")
do
    # Switch to the next subscription on the loop
    az account set --subscription ${subs}
    az group list -o table --query "[].{Subscription:'${subs}',Name:name,Location:location}"
done
```

So now, the output looks like this:

```
Subscription                          Name                        Location
------------------------------------  --------------------------  ----------------
dxxxxxxe-1234-5678-9012-345678901234  flowable-Ahsh8At5           westeurope
dxxxxxxe-1234-5678-9012-345678901234  DefaultResourceGroup-WEU    westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-deej1Ood           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-Ahs6ja8u           westeurope
dxxxxxxe-1234-5678-9012-345678901234  AzureBackupRG_westeurope_1  westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-AHohk4pu           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-on8ooJai           westeurope
dxxxxxxe-1234-5678-9012-345678901234  flowable-aiKai6Ku           switzerlandnorth
dxxxxxxe-1234-5678-9012-345678901234  NetworkWatcherRG            brazilsouth
```

## Remove unwanted (and repeated) Resource Groups

Looks like Azure creates some Resource Groups by default, and yes, I'm talking about:
* [DefaultResourceGroup-XXX](https://docs.microsoft.com/en-us/answers/questions/690633/defaultresourcegroup-cus-keeps-getting-created.html)
* [AzureBackupRG_location_X](https://docs.microsoft.com/en-us/azure/backup/backup-during-vm-creation#azure-backup-resource-group-for-virtual-machines)
* [NetworkWatcherRG](https://docs.microsoft.com/en-us/answers/questions/27211/what-is-the-networkwatcherrg.html)

All those Resource Groups are created and maintained by Azure, you can delete them or rename them but, eventually, Azure will re-create them or you must reconfigure the way you deploy your infrastructure. Anyhow, we don't need them for this report, so, let's try to remove them from the report.

It turns out that JMESPath the "thingy" behind the `az cli` query function allows you to filter the results of your query before showing the results.  
The most useful function is `search` but there are some other functions _i.a._ `starts_with`, `ends_with`, `contains` and you can apply logical operators, _i.a_ `< > == !=`  

So, let's try to get the list of Resource Groups which their name is not any of the defaults one:


```bash
for subs in $(az account list -o tsv --query "[].id")
do
    # Switch to the next subscription on the loop
    az account set --subscription ${subs}
    az group list -o table --query "[? !(contains(name,'DefaultResourceGroup')) && !(contains(name,'NetworkWatcherRG')) && !(contains(name,'AzureBackupRG'))].{Subscription:'${subs}',Name:name,Location:location}"
done
```

The query could be read something like:

```Search all the RG which name:
   - doesn't contain 'DefaultResourceGroup'
   - _AND_ doesn't contain 'NetworkWatcherRG'
   - _AND_ doesn't contain 'AzureBackupRG' 

   - Once you get that list show only the subscription, the RG Name and the RG Location
```


Enjoy!

----

## Further readings and sources.

* [Output formats for Azure CLI commands](https://docs.microsoft.com/en-us/cli/azure/format-output-azure-cli)
* [How to query Azure CLI command output using a JMESPath query](https://docs.microsoft.com/en-us/cli/azure/query-azure-cli?tabs=concepts%2Cbash)
* [JMESPath queries - Azure Citadel](https://www.azurecitadel.com/cli/jmespath/#selecting-object-values)

## Closing

I'd like to thank you all the people who wrote the aforementioned documents who left the breadcrumbs that allowed me to end here. :-)

----

[^1]: I used "Advance" here since the queries and the functions used here are not very common. Forgive the arrogance of using that term.
